#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "GridGrouping.h"
#include <stdint.h>

using namespace cv;
using namespace std;

char key;
const int8_t alpha_slider_max = 200;
int8_t alpha_slider;
int16_t SADWindowSize;

// change to see what you want to display 
bool displaySliders = true;
bool displayOtherImages = true;
bool displayQuadLines = true;
bool displaySelectedQuad = true;

int iSliderValue1 = 3; // 6

int iSliderValue2 = 0; // 4
int iSliderValue3 = 0; // 5
int iSliderValue4 = 62; // 51
int iSliderValue5 = 0; // originally -64
int iSliderValue6 = 0; // 15
int iSliderValue7 = 0; // 25
int iSliderValue8 = 0; // 1 
int iSliderValue9 = 0; // 32
int iSliderValue10 = 28; // 15

int main(int argc, char** argv)
{
	//	cvNamedWindow("Camera_Output", 1);    //Create window
	//	cvNamedWindow("Second Camera Output", 1);

	// Initialize variables
	// Capture from connected cameras 
	CvCapture* capture = cvCaptureFromCAM(0); 
	CvCapture* secondCapture = cvCaptureFromCAM(2);

	IplImage* frame;
	IplImage* secondFrame;

	Mat display, display8, sliders;
	Mat disparity_left, disparity_right;

	//Create infinite loop for live streaming
	while (1){ 

		frame = cvQueryFrame(capture); //Create image frames from capture
		secondFrame = cvQueryFrame(secondCapture);
		//	cvShowImage("Camera_Output", frame);   //Show image frames on created window
		//	cvShowImage("Second Camera Output", secondFrame);
		
		Mat g1(frame);
		Mat g2(secondFrame);
		//Mat g1 = imread("left.png",CV_LOAD_IMAGE_COLOR);
		//Mat g2 = imread("right.png", CV_LOAD_IMAGE_COLOR);

		cvtColor(g1, g1, COLOR_BGR2GRAY);
		cvtColor(g2, g2, COLOR_BGR2GRAY);
		//		cvtColor(leftImage,leftImage, COLOR_BGR2GRAY);
		//		cvtColor(rightImage,rightImage, COLOR_BGR2GRAY);

		/*namedWindow("left", WINDOW_AUTOSIZE);
		namedWindow("right", WINDOW_AUTOSIZE);
		namedWindow("disp", WINDOW_AUTOSIZE);*/

		if (displaySliders) {
			namedWindow("sliders", CV_WINDOW_NORMAL);

			createTrackbar("SADWindowSize", "sliders", &iSliderValue1, 125);
			createTrackbar("numOfDisparities", "sliders", &iSliderValue2, 15);
			createTrackbar("preFilterSize", "sliders", &iSliderValue3, 125);
			createTrackbar("preFilterCap", "sliders", &iSliderValue4, 62);
			createTrackbar("minDisparity", "sliders", &iSliderValue5, 100);
			createTrackbar("textureThres", "sliders", &iSliderValue6, 100);
			createTrackbar("uniquenessRatio", "sliders", &iSliderValue7, 50);
			createTrackbar("speckWinSize", "sliders", &iSliderValue8, 200); // btwn 50 and 200
			createTrackbar("speckleRange", "sliders", &iSliderValue9, 5);
			createTrackbar("disp12MaxDiff", "sliders", &iSliderValue10, 100);
		}

		int8_t iSADWindowSize = 6 + (iSliderValue1 * 2) - 1; // from 2 to 15 (odd)
		int8_t inumOfDisparities = 16 + iSliderValue2 * 16; // from 2 to 15 (odd)
		int8_t ipreFilterSize = 6 + (iSliderValue3 * 2) - 1; // from 2 to 15 (odd)
		int8_t ipreFilterCap = 1 + iSliderValue4; // from 2 to 15 (odd)
		int8_t iminDisparity = -1 * iSliderValue5; // from 2 to 15 (odd)
		int8_t itextureThreshold = iSliderValue6; // from 2 to 15 (odd)
		int8_t iuniquenessRatio = iSliderValue7; // from 2 to 15 (odd)
		int8_t ispeckleWindowSize = iSliderValue8; // from 2 to 15 (odd)
		int8_t ispeckleRange = iSliderValue9 * 16; // from 2 to 15 (odd)
		int8_t idisp12MaxDiff = iSliderValue10; // from 2 to 15 (odd)

		StereoBM sbm;
		sbm.state->SADWindowSize = iSADWindowSize;
		sbm.state->numberOfDisparities = inumOfDisparities;
		sbm.state->preFilterSize = ipreFilterSize;
		sbm.state->preFilterCap = ipreFilterCap;
		sbm.state->minDisparity = iminDisparity;
		sbm.state->textureThreshold = itextureThreshold;
		sbm.state->uniquenessRatio = iuniquenessRatio;
		sbm.state->speckleWindowSize = ispeckleWindowSize;
		sbm.state->speckleRange = ispeckleRange;
		sbm.state->disp12MaxDiff = idisp12MaxDiff;
		sbm(g1, g2, display);

		// sbm(leftImage, rightImage, display);
		normalize(display, display8, 0, 255, CV_MINMAX, CV_8U);

		// run obstacle avoidance algorithm
		GridGroup g(display8, displayOtherImages); 
		g.runAvoidance();

		if (displayOtherImages){
			imshow("left", g1);
			imshow("right", g2);
		}
		imshow("disp", display8);
		key = cvWaitKey(100);     //Capture Keyboard stroke

		if (char(key) == 27){
			break;      //If you hit ESC key loop will break.
		}
	}

	//capture relese; outside infinite loop
	//cvReleaseCapture(&capture);
	//cvReleaseCapture(&secondCapture);

	//cvDestroyWindow("Camera_Output"); //Destroy Window
	//cvDestroyWindow("Second Camera Output");
	return 0;
}
