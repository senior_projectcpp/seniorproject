int numPixelsWide = 1200; // completely arbitrary numbers
int numPixelsHigh = 800; // completely arbitrary numbers

double TL_pixels = 0.0;
double TR_pixels = 0.0;
double BL_pixels = 0.0;
double BR_pixels = 0.0;

// CAPITAL comments are essentially function calls or representing smaller snippets of code
// uncapitalized comments are purely comments

// When you see "CHECK PIXEL", this is described in pseudocode at the bottom of this file.


for(int row = 0; row < numPixelsHigh; row++)
{
	for(int col = 0; col < numPixelsWide; col++)
	{
		if(((row < (numPixelsHigh / 2)) && (col < numPixelsWide / 3))
			|| ((row < (numPixelsHigh / 3)) && (col < numPixelsWide / 2)))
			{
				// Top Left Quadrant aka TL
				// CHECK PIXEL, IF OF INTEREST
					// CHECK SURROUNDING PIXELS, IF HALF FULL
						// ADD to TL_pixels
			}
		else if(((row < (numPixelsHigh / 2)) && (col > numPixelsWide / 3))
			|| ((row < numPixelsHigh / 3)) && (col > numPixelsWide / 2)))
			{
				// Top Right Quadrant aka TR
				// CHECK PIXEL, IF OF INTEREST
					// CHECK SURROUNDING PIXELS, IF HALF FULL
						// ADD to TR_pixels
			}
		else if(((row > (numPixelsHigh / 2)) && (col < numPixelsWide / 3))
			|| ((row > (numPixelsHigh / 3)) && (col < numPixelsWide / 2)))
			{
				// Bottom Left Quadrant aka BL
				// CHECK PIXEL, IF OF INTEREST
					// CHECK SURROUNDING PIXELS, IF HALF FULL
						// ADD to BL_pixels
			}
		else if(((row > (numPixelsHigh / 2)) && (col > numPixelsWide / 3))
			|| ((row > numPixelsHigh / 3)) && (col > numPixelsWide / 2)))
			{
				// Bottom Right Quadrant aka BR
				// CHECK PIXEL, IF OF INTEREST
					// CHECK SURROUNDING PIXELS, IF HALF FULL
						// ADD to BR_pixels
			}
		else
		{
			// Center Quadrant
			// Now we need to divide it into a grid of 3x3
			
			if((row > (numPixelsHigh / 3)) && (row < ((numPixelsHigh * 4) / 9)))
			{
				// Top Row of Center Quadrant
				if((col > (numPixelsWide / 3)) && (col < ((numPixelsWide * 4) / 9)))
				{
					// Center Quadrant Top Left aka C_TL
				}
				else if((col >= ((numPixelsWide * 4) / 9)) && (col <= ((numPixelsWide * 5) / 9)))
				{
					// Center Quadrant Top Middle aka C_TM
				}
				else
				{
					// Center Quadrant Top Right aka C_TR
				}
			}
			else if((row >= ((numPixelsHigh * 4) / 9)) && (row <= ((numPixelsHigh * 5 ) / 9)))
			{
				// Middle Row of Center Quadrant
				if((col > (numPixelsWide / 3)) && (col < ((numPixelsWide * 4) / 9)))
				{
					// Center Quadrant Middle Left aka C_ML
				}
				else if((col >= ((numPixelsWide * 4) / 9)) && (col <= ((numPixelsWide * 5) / 9)))
				{
					// Center Quadrant Middle Center aka C_MM
				}
				else
				{
					// Center Quadrant Middle Right aka C_MR
				}
			}
			else
			{
				// Bottom row of Center Quadrant
				if((col > (numPixelsWide / 3)) && (col < ((numPixelsWide * 4) / 9)))
				{
					// Center Quadrant Bottom Left aka C_BL
				}
				else if((col >= ((numPixelsWide * 4) / 9)) && (col <= ((numPixelsWide * 5) / 9)))
				{
					// Center Quadrant Bottom Middle aka C_BM
				}
				else
				{
					// Center Quadrant Bottom Right aka C_BR
				}
			}
		}
	}
}

// Once we have counted up the pixels in each quadrant,
// determine the least occupied quadrant
// BUT
	// IF More than one quadrant is determined to be occupied,
	// We need preset preferences
		// For example, if TL and BR are occupied, just go TR.
		// Or if TL and TR are occupied, go directly down, do not pick least occupied quadrant
		// etc.
// Once we have established the best direction
// SET NEW DIRECTION

// The same concept applies to the center quadrant,
// once the most occupied center quadrant has been determined, stray away from it
// If more than one center quadrant is occupied, drastically increase manuevering speed and head towards best choice (choice being decisions we have pre-established)
// 


// CHECKING A PIXEL
// Convert pixel grayscale to float (or double) value between 0 (completely black) and 1 (completely white)
// If this value is of predetermined interested (tweakable value, probably around 0.25 or 0.5)
	// Then
	// Check pixels surrounding and if half or more are also of interest
		// Then add this float value (somewhere between 0 and 1 to that quadrant)



