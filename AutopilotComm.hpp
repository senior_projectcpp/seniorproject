#ifndef _AutopilotComm_h_
#define _AutopilotComm_h_

int32_t sendWaypoint(CASData_t* AvoidanceWaypoint );
int32_t updateACState(CASData_t* AircraftState );
int32_t beginMAVComm();
int32_t endMAVComm();

#endif