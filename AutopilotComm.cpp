#include <iostream>
#include <mavlink.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include "CAS_msg.hpp"
#include "AutopilotComm.hpp"
using namespace std;

int32_t sendWaypoint(CASData_t* AvoidanceWaypoint )
{
	// Extract data from AvoidanceWaypoint
	lon = AvoidanceWaypoint.X; // Longitude
	lat = AvoidanceWaypoint.Y; // Latitude
	alt = AvoidanceWaypoint.Z; // Altitude

	// Target Autopilot Setup
	uint8_t _target_system = 178; // Target id of autopilot
	uint8_t _target_component = 0; // Target component of 0 is all

	uint16_t seq = 0; // Sequence is always 0
	uint8_t frame = MAV_FRAME_GLOBAL; // Set the frame to global
	uint16_t command = MAV_CMD_NAV_WAYPOINT; // Command the APM for waypint
	uint8_t current = 2; // Waypoint mode
	uint8_t autocontinue = 0; // Always 0
	float param1 = 0; // 0 for fixed wing
	float param2 = 1; // Range from waypoint (meters)
	float param3 = 0; // Pass through waypoint
	float param4 = 0l // Desired yaw angle

	// Initialize the buffers
	mavlink_message_t msg;
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];

	// Pack the message
	mavlink_msg_mission_item_pack(sysid,compid,&msg,_target_system,_target_component,seq,frame,command,current,autocontinue,param1,param2,param3,param4,lat,lon,alt);

	// Copy to send buffer
	uint16_t len = mavlink_msg_to_send_buffer(buf,&msg);

	if(uart0_filestream != -1)
	{
		int count = write(uart0_filestream, &buf[0], &buf[len-1]);
		if(count < 0)
		{
			cout << "UART TX error" << endl;
		}
		else
		{
			cout << "Waypoint Sent Successfully" << endl;
		}
	}
	else
	{
		cout << "UART filestream not initilized" << endl;
	}
	return 0;
}

int32_t updateACState(CASData_t* AircraftState )
{
	// Code Code Code Code
}

int32_t beginMAVComm()
{
	// Open the UART
	uart0_filestream = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);

	if(uart0_filestream == -1)
	{
		cout << "UART Error - Cannot open UART" << endl;
		return -1;

	}

	struct termios options;
	tcgetattr(uart0_filestream, &options);
	options.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
	options.c_iflag = IGNPAR;
	options.c_oflag = 0;
	options.c_lflag = 0;
	tcflush(uart0_filestream, TCIFLUSH);
	tcsetattr(uart0_filestream, TCSANOW, &options);

	cout << "UART Initialized"

	return 0;
}

int32_t endMAVComm()
{
	if(uart0_filestream != -1)
	{
		close(uart0_filestream)
		cout << "UART Terminated!" << endl;
	}
	else
	{
		cout << "UART stream already closed"
	}
	return 0;
}