/**************************************
*  Collision Avoidance Message Header *
* 									  *
**************************************/
#ifndef _CAS_msg_h_
#define _CAS_msg_h_

typedef struct
{
	float X; // Longitude position
	float Y; // Latitude position
	float Z; // Altitude
	float HDG; // Heading
	float VEL; // Velocity
	float ROC; // Rate of Climb (positive up)
} CASData_t;

#endif