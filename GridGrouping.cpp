 /*
 ************************************************************************************************************************* 
 GridGrouping.cpp
 Obstacle avoidance algorithm
 Developed by: Thirupathi Srinivasan, Justin Gray, Nathan McDorman, Mark Torstenbo, Jorge Corral, Nathan Brown, Justin Wood
 2014-15 Senior Project
 Advisor: Dr.Bhandari
 Property of California State Polytechnic University, Aerospace Engineering Dept.
 Release not authorized without approval
 ************************************************************************************************************************
 */

#include "GridGrouping.h";

/*
 CAPITAL comments are essentially function calls or representing smaller snippets of code
 uncapitalized comments are purely comments

 When you see "CHECK PIXEL", this is described in pseudocode at the bottom of this file.
 img.at outputs value between 0-255 where 0 would be black and 255 would be white
 may want to use blob detection, here is a link: http://www.learnopencv.com/blob-detection-using-opencv-python-c/

 may need to create a 3d point cloud: http://blog.martinperis.com/2012/01/3d-reconstruction-with-opencv-and-point.html
                                      https://erget.wordpress.com/2014/04/27/producing-3d-point-clouds-with-a-stereo-camera-in-opencv/
 openCV Docs:
 Mat: http://docs.opencv.org/modules/core/doc/basic_structures.html
 Point: http://docs.opencv.org/doc/user_guide/ug_mat.html
 Basic Drawing: http://docs.opencv.org/doc/tutorials/core/basic_geometric_drawing/basic_geometric_drawing.html
 */

// Constructor
GridGroup::GridGroup(Mat &img, bool display) : image(img) { 
	showHUD = display;
	if (display){
		hud = img.clone();
	}
}

// set image size
// must be called to reset image size, or will resort to default values (1200 x 800)
void GridGroup::setImageSize(){
	numPixelsHigh = image.rows;
	numPixelsWide = image.cols;
}

// display HUD over selected image
void GridGroup::displayHUD(){

	// Horizontal Lines
	pt1.x = 0;
	pt1.y = numPixelsHigh / 3;
	pt2.x = numPixelsWide;
	pt2.y = numPixelsHigh / 3;
	line(hud, pt1, pt2, Scalar(255, 255, 255), 1, 8);

	pt3.x = 0;
	pt3.y = 2*numPixelsHigh / 3;
	pt4.x = numPixelsWide;
	pt4.y = 2*numPixelsHigh / 3;
	line(hud, pt3, pt4, Scalar(255, 255, 255), 1, 8);

	// Vertical Lines
	pt5.x = numPixelsWide / 3;
	pt5.y = 0;
	pt6.x = numPixelsWide / 3;
	pt6.y = numPixelsHigh;
	line(hud, pt5, pt6, Scalar(255, 255, 255), 1, 8);

	pt7.x = 2* numPixelsWide / 3;
	pt7.y = 0;
	pt8.x = 2* numPixelsWide / 3;
	pt8.y = numPixelsHigh;
	line(hud, pt7, pt8, Scalar(255, 255, 255), 1, 8);
}

// check Pixel Intensities
void GridGroup::recordAvgPixelIntensities() {
	Mat i (numPixelsHigh,numPixelsWide,CV_8U,Scalar(0,0,0));
	/*
	*************************** LEVEL 2 AVOIDANCE -- BEGIN *********************************
	// detect blobs
	//SimpleBlobDetector detector(blobParams);
	//detector.detect(image, keypoints);

	// demark blobs with red circles
	//drawKeypoints(image, keypoints, image, Scalar(255, 255, 255), DrawMatchesFlags::DEFAULT);
	/************************** LEVEL 2 AVOIDANCE -- END ***********************************
	*/

	// Split Image into 9 quadrants 
	// cout << "NumPixelWide = " << numPixelsWide;
	// cout << "NumPixelHigh = " << numPixelsHigh << '\n';

	// Increment pixel intensity in each quadrant
	int rowIndex = 0;
	for (int row = 0; row < numPixelsHigh; row++)
	{
		if (row < (numPixelsHigh / 3)) { rowIndex = 0; } // Top Row
		else if ((row >(numPixelsHigh / 3)) && (row < (2 * numPixelsHigh / 3))) { rowIndex = 1; } // Middle Row
		else { rowIndex = 2;} // Bottom Row

		for (int col = 0; col < numPixelsWide; col++)
		{

			Scalar intensity = image.at<uchar>(Point(col,row));
			
			if (intensity[0] >= intensityThreshold) {
				if (col < (numPixelsWide / 3)) {
					// Left Column
					pixelIntensityArr[rowIndex][0] += (intensity[0]);
					pixelCountArr[rowIndex][0]++;
				}
				else if ((col > (numPixelsWide / 3)) && (col < (2 * numPixelsWide / 3))) {
					// Middle Column
					pixelIntensityArr[rowIndex][1] += (intensity[0]);
					pixelCountArr[rowIndex][1]++;
				}
				else {
					// Right Column
					pixelIntensityArr[rowIndex][2] += (intensity[0]);
					pixelCountArr[rowIndex][2]++;
				}

				pt1.x = col;
				pt1.y = row;
				line(i, pt1, pt1, Scalar(255, 255, 255), 2, 8);
			}
			//cout << "Row: "<< row << " Col: " << col << " I: " << intensity << "\n";
		}
		imshow("Above Threshold", i);
	}
	
	// Find Average Pixel Intensity in Each Quadrant
	for (int row = 0; row < 3; row++){
		for (int col = 0; col < 3; col++){
			pixelIntensityArr[row][col] = (pixelIntensityArr[row][col]) / (pixelCountArr[row][col]);
		}
	}

	cout << "[{" << pixelIntensityArr[0][0] << ',' << pixelIntensityArr[0][1] << ',' << pixelIntensityArr[0][2] << "} \n";
	cout << " {" << pixelIntensityArr[1][0] << ',' << pixelIntensityArr[1][1] << ',' << pixelIntensityArr[1][2] << "} \n";
	cout << " {" << pixelIntensityArr[2][0] << ',' << pixelIntensityArr[2][1] << ',' << pixelIntensityArr[2][2] << "}] \n";
}

// Select Most Vacant Quadrant 
void GridGroup::selectVacantQuadrant(){
	int val = pixelIntensityArr[0][0];
	for (int row = 0; row < 3; row++){
		for (int col = 0; col < 3; col++){
			if (pixelIntensityArr[row][col] < val){
				selectedRow = row;
				selectedCol = col;
				val = pixelIntensityArr[row][col];
			}
		}
	}
	cout << selectedRow << selectedCol;
}

// Display Most Vacant Quadrant 
void GridGroup::displaySelectedQuad(){
	// draw rectangle around chosen quadrant
	pt1.x = selectedCol*(numPixelsWide/3);
	pt1.y = selectedRow*(numPixelsHigh/3);
	pt2.x = (selectedCol+1)*(numPixelsWide / 3);
	pt2.y = (selectedRow+1)*(numPixelsHigh / 3);
	rectangle(hud, pt1, pt2, Scalar(255, 255, 255), 5, 8);
	cout << pt1.x << pt1.y;
}

// Choose Waypoint
void GridGroup::createNewWaypoint(){
	// Create a waypoint based on selected quadrant
	// FOV = 60 deg. from edge to edge horizontally
	// Waypoint created to midpoint of quadrant

	// set coordinate axis origin to center quadrant and measure angles relative to there
	headingChange = (selectedCol - 1) * (fovHoriz/2);
	newHeading = /* get Heading + */ headingChange;
	incChange = (selectedRow - 1) * (fovVert / 2); 
	newInc = /* get Inclination + */ incChange;
	newLongitude = /*Get Longitude Fxn + */ wpDist*sin(newHeading*PI / 180.0);
	newLatitude = /*Get Latitude Fxn + */ wpDist*cos(newHeading*PI / 180.0);
	newAlt = /*Get Altitude Fxn + */ wpDist*sin(newInc*PI / 180.0);

	cout << "Heading Change:  " << headingChange << '\n';
	cout << "Inclination Change:  " << incChange << '\n';
}

// run obstacle avoidance algorithm
void GridGroup::runAvoidance() {
	setImageSize();
	recordAvgPixelIntensities();
	selectVacantQuadrant();
	if (showHUD){
		displayHUD();
		displaySelectedQuad();
		imshow("Image with HUD", hud);
	}
	createNewWaypoint();
}


