#include <iostream>
#include "AutopilotComm.hpp"
#include "AutopilotComm.cpp"
#include "CAS_msg.hpp"
using namespace std;

int main()
{
	CASData_t waypoint;
	waypoint.X = -135.5;
	waypoint.Y = 33.0;
	waypoint.Z = 50;
	
	sendWaypoint(waypoint);

	return 0;
}
