// GridGrouping.h
// Header file with relevant functions

#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <cstdint>
#include <math.h>

using namespace std;
using namespace cv;

class GridGroup {

public:
	int numPixelsWide = 1200; // completely arbitrary numbers
	int numPixelsHigh = 800; // completely arbitrary numbers
	int intensityThreshold = 100; // threshold value
	int selectedRow = 0;
	int selectedCol = 0;
	int pixelIntensityArr [3][3] = {};
	int pixelCountArr[3][3] = {};

	// weightage for the location to place waypoint in image frame
	float vacantQuadWeight = 0.6;
	float oppositeXYWeight = 1-vacantQuadWeight; 

	GridGroup(Mat &image, bool display);// constructor
	void setImageSize(); // get image pixel height and width
	void displayHUD(); // display HUD
	void displaySelectedQuad(); // display Selected Quad
	void recordAvgPixelIntensities(); // begin grid grouping
	void createNewWaypoint();
	void runAvoidance();

	// Waypoint Parameters
	// Horizontal and Vertical camera FOV
	int fovHoriz = 60;
	int fovVert = 60;
	double wpDist = 100.0; // distance to set to next waypoint
	double headingChange, newHeading, incChange, newInc, newLongitude, newLatitude, newAlt;

private:
	Mat &image, hud;
	Point pt1,pt2,pt3,pt4,pt5,pt6,pt7,pt8; // point ends for HUD lines
	bool showHUD;
	// SimpleBlobDetector::Params blobParams; // deprecated; use for blob detection
	//vector <KeyPoint> keypoints;
	void selectVacantQuadrant();
	double const PI = 3.14159265358979;
};

